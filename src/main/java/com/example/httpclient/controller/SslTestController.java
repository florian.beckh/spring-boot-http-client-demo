package com.example.httpclient.controller;

import com.azure.security.keyvault.jca.KeyVaultLoadStoreParameter;
import lombok.AllArgsConstructor;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.PrivateKeyDetails;
import org.apache.http.ssl.PrivateKeyStrategy;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import java.io.IOException;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.Map;

@RestController
public class SslTestController {

    @Value("${azure.keyvault.uri}")
    private String keyVaultUri;

    @Value("${azure.keyvault.tenant-id}")
    private String keyVaultTenantId;

    @Value("${azure.keyvault.client-id}")
    private String keyVaultClientId;

    @Value("${azure.keyvault.client-secret}")
    private String keyVaultClientSecret;

    @Value("${application.client.cert}")
    private String countryCertAlias;

    @GetMapping(value = "/")
    public String outbound() throws GeneralSecurityException, IOException {
        var azureKeyVaultStore = KeyStore.getInstance("AzureKeyVault");
        var parameter = new KeyVaultLoadStoreParameter(
                keyVaultUri,
                keyVaultTenantId,
                keyVaultClientId,
                keyVaultClientSecret);
        azureKeyVaultStore.load(parameter);
        SSLContext sslContext = SSLContexts.custom()
                .loadTrustMaterial(azureKeyVaultStore, null)
                .loadKeyMaterial(azureKeyVaultStore, "".toCharArray(), new ClientPrivateKeyStrategy( countryCertAlias ))
                .build();
        var socketFactory = new SSLConnectionSocketFactory(sslContext,
                (hostname, session) -> true);
        var httpClient = HttpClients.custom()
                .setSSLSocketFactory(socketFactory)
                .build();
        var requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        var sslTest = String.format("https://localhost:%s/ssl-test", 40000);
        var restTemplate = new RestTemplate(requestFactory);

        var response
                = restTemplate.getForEntity(sslTest, String.class);

        return "Outbound TLS " +
                (response.getStatusCode() == HttpStatus.OK ? "is" : "is not")  + " Working!!";
    }

    private static class ClientPrivateKeyStrategy implements PrivateKeyStrategy {

        private final String certAlias;

        public ClientPrivateKeyStrategy( final String certAlias ) {
            this.certAlias = certAlias;
        }

        @Override
        public String chooseAlias(Map<String, PrivateKeyDetails> map, Socket socket) {
            return this.certAlias;
        }
    }
}
